import React, { ReactElement } from 'react'
import { Button } from 'reactstrap'
import "./Btn.scss"

interface Props {
    isLoading?: boolean;
    disabled?: boolean;
    onClick?: (ev: any) => void;
    className?: string;
    color: string;
    text?: string;
    children?: ReactElement;
}

export default function Btn({ isLoading = false, onClick, className, color, text, disabled, children}: Props) {
  return (
    <Button
        onClick={onClick && onClick}
        color={color ? color : 'primary'}
        className={`customBtn ${className ? className : ""}`}
        disabled={disabled}
    >
        
        {isLoading ? (
            <div className="spinner-border text-light" role="status">
                <span className="sr-only"></span>
            </div>
        ) : null}
        <span className={isLoading ? "opacity-zero" : ""}>{text ? text : children}</span>
        
    </Button>
  )
}
