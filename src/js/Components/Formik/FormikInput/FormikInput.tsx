import { useField } from "formik";
import React, { useEffect } from 'react';
import ReactFlagsSelect from "react-flags-select";
import PhoneInput, { getCountryCallingCode } from 'react-phone-number-input';
import 'react-phone-number-input/style.css';
import "./FormikInput.scss";

interface Props {
    label: string;
    name: string;
    countryCode?: string;
    defaultCountry?: string;
    setCountryCode?: (code: string) => any
    [props:string]: any;
}

const FormikInput = ({ label, countryCode, setCountryCode, defaultCountry, ...props }: Props) => {
    const [field, meta] = useField(props);

    useEffect(() => {
        if(props?.type === "phone" && (countryCode || defaultCountry)){
            //@ts-ignore
            const callingCode =countryCode ? getCountryCallingCode(countryCode) : defaultCountry ? getCountryCallingCode(defaultCountry) : ""
            props?.setFieldValue(props?.name, "+" + callingCode)
        }
    }, [countryCode, defaultCountry])

    return (
      <div className='form-group mt-3'>
            <label className="mb-1" htmlFor={props.id || props.name}>{label}</label>
            {props?.type === "country-select" ? (
                <ReactFlagsSelect
                    {...field}
                    {...props}
                    selected={countryCode ? countryCode : ""}
                    onSelect={(code: string) => {
                        setCountryCode && setCountryCode(code)
                        props?.setFieldValue(props?.name, code)
                    }}
                    className={`react-flags-select ${props?.className ? props?.className : ""}`}
                />
            ) : props?.type === "phone" ? (
                <PhoneInput
                    {...field}
                    {...props}
                    //@ts-ignore
                    defaultCountry={defaultCountry}
                    country={countryCode}
                    onChange={(phone: string) => {
                        props?.setFieldValue(props?.name, phone)
                    }}
                    international
                    className='form-control'
                />
            ) : (
                <input
                    {...field}
                    {...props}
                    className={`form-control ${props?.className ? props?.className : ""}`}
                    onClick={function(ev: any){
                        if(props?.type === "date"){
                            ev?.target?.showPicker()
                        }
                        props?.onClick && props?.onClick()
                    }}
                />
            )}
            {meta.touched && meta.error ? (
            <div className="text-danger">{meta.error}</div>
            ) : null}
      </div>
    );
  };

  
export default FormikInput