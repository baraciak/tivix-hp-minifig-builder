import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Alert as ReactAlert } from 'reactstrap';
import { openAlert } from '../../store/actions';
import "./Alert.scss";

export default function Alert() {
    const alert = useSelector((state: any) => state?.Alert);
    const dispatch = useDispatch();

    const onOpenAlert = (text: string, variant: string) => dispatch(openAlert(text, variant));  

    useEffect(() => {
        if(alert?.text){
            setTimeout(() => {
                onOpenAlert("", "")
            }, 6000)
        }
    }, [alert?.text])

    return (
        alert?.text ? (
            <ReactAlert color={alert?.variant}>
                {alert?.text}
            </ReactAlert>
        ) : null
  )
}
