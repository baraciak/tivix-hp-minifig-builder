import React from 'react'
import { Link } from 'react-router-dom'
import Btn from '../../Components/Btn/Btn'
import './ErrorPage.scss'

const ErrorPage = () => {
  return (
    <div className='page-container'>
        <div className="not-found-status font-harry-potter">404</div>
        <h1 className="homepage-title font-harry-potter">Not Found</h1>
        <Link to="/"><Btn color="primary" text={"Take Me Home"}/></Link>
    </div>
  )
}

export default ErrorPage
