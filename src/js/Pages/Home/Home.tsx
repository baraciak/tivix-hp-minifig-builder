import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import Btn from '../../Components/Btn/Btn'
import { resetAppState } from '../../store/actions'

const Home = () => {
  const dispach = useDispatch()

  useEffect(() => {
    dispach(resetAppState());
  }, [])
  
  return (
    <div className='page-container'>
        <h1 className="homepage-title font-harry-potter">LEGO MINIFIGS MISTERY BOX</h1>
        <Link to="/start">
          <Btn color="primary" text={"Start"} />
        </Link>
    </div>
  )
}

export default Home
