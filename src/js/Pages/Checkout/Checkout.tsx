import { Formik } from "formik"
import React, { useEffect, useState } from 'react'
import { useDispatch } from "react-redux"
import { useNavigate, useParams } from 'react-router-dom'
import { Col, Row } from "reactstrap"
import * as Yup from 'yup'
import "yup-phone-lite"
import Btn from "../../Components/Btn/Btn"
import FormikInput from '../../Components/Formik/FormikInput/FormikInput'
import Tile from '../Tiles/Tile/Tile'
import { createOrder, getMinifigDetails, getMinifigParts } from '../../helpers/api'
import { openAlert } from "../../store/actions"
import "./Checkout.scss"

interface FormValues {
        name: string;
        surname: string;
        phone: string;
        email: string;
        "date-of-birth": string;
        'country-code': string;
        address: string;
        city: string;
        state: string;
        zipcode: string;
}

const Checkout = () => {
    const { minifig_id } = useParams()
    const [minifigDetails, setMinifigDetails] = useState<any>({})
    const [minifigParts, setMinifigParts] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [formIsValid, setFormIsValid] = useState(false)
    const [countryCode, setCountryCode] = useState("")
    const [isFormLoading, setIsFormLoading] = useState(false);
    const [isSubmitted, setIsSubmitted] = useState(false);
    const dispatch = useDispatch();
    const onOpenAlert = (text: string, variant: string) => dispatch(openAlert(text, variant));
    const navigate = useNavigate();

    useEffect(() => {
        if(minifig_id) {
            getDetails(minifig_id)
        }
    }, [minifig_id])

    const getDetails = async (minifig_id: string) => {
        try {
            setIsLoading(true)

            if(!minifig_id){
                throw {};
            }

            await handleGetMinifigDetails(minifig_id)
            await handleGetMinifigParts(minifig_id)
        } catch(e: any) {
            console.log("getDetails Error")
            onOpenAlert(e?.response?.data?.detail ? e?.response?.data?.detail : "Minifig Not Found.", "danger")
            navigate("/not-found")
        } finally {
            setIsLoading(false)
        }
    }

    const handleGetMinifigDetails = async (minifig_id: string) => {
        try {
            setIsLoading(true)
            const response: any = await getMinifigDetails(minifig_id);
            // console.log(["getMinfigDetails", response])
            if(response?.status === 200){
                setMinifigDetails(response?.data)
            }
        } catch(e: any) {
            console.log("handleGetMinifigDetails Error", )
            throw e;
        }
    }

    const handleGetMinifigParts = async (minifig_id: string) => {
        try {
            setIsLoading(true)
            const response: any = await getMinifigParts(minifig_id);
            // console.log(["getMinifigParts", response])
            if(response?.status === 200){
                setMinifigParts(response?.data?.results);
            }
        } catch(e: any) {
            console.log("handleGetMinifigParts Error", e)
            throw e;
        }
    }

    const handleValidSubmit = async (values: FormValues) => {
        try{
            setIsFormLoading(true);
            const data = { ...values, minifig_id: minifigDetails?.set_num }
            const response: any = await createOrder(data);
            
            if(response?.status === 200){
                onOpenAlert(response?.data?.message, response?.data?.status)
                setTimeout(() => navigate("/"), 2500)
            }
            setIsSubmitted(true)
        } catch(err: any) {
            setIsSubmitted(false)
            onOpenAlert(err?.data?.message, err?.data?.status)
        } finally {
            setIsFormLoading(false);
        }
    }

    return (
        <div className='page-container'>
            {!isLoading ? (
                <Formik
                    initialValues={{
                        name: "",
                        surname: "",
                        phone: "",
                        email: "",
                        "date-of-birth": "",
                        'country-code': "",
                        address: "",
                        city: "",
                        state: "",
                        zipcode: ""
                    }}
                    onSubmit={handleValidSubmit}
                    isInitialValid={false}
                    validationSchema={
                        Yup.object().shape({
                            name: Yup.string().required("Name is required."),
                            surname: Yup.string().required("Surname is required."),
                            // @ts-ignore
                            phone:  Yup.string().phone(countryCode, "This is not a correct phone for selected country.").required("Phone is required."),
                            email: Yup.string().email("This is not an email.").required("Email is required."),
                            'date-of-birth': Yup.date(),
                            'country-code': Yup.string().required("Country is required."),
                            address: Yup.string().required("Address is required."),
                            city: Yup.string().required("City is required."),
                            state: Yup.string().required("State is required."),
                            zipcode: Yup.string().required("Zipcode is required.")
                        })
                    }
                >
                    {props => {
                        const {
                            isValid,
                            handleSubmit,
                            isSubmitting,
                            isValidating
                        } = props;
                        // console.log(props)
                        setFormIsValid(isValid)
                        return (
                            <Row className="checkout-container">
                                <Col md={9}>
                                    <div className='form-container'>
                                        <h1 className="homepage-title font-harry-potter">Shipping Details</h1>
                                        
                                        <Row>
                                            <Col md={6}>
                                                <FormikInput
                                                    label="Name"
                                                    name="name"
                                                    type="name"
                                                    placeholder="Harry"
                                                />
                                            </Col>
                                            <Col md={6}>
                                                <FormikInput
                                                    label="Surname"
                                                    name="surname"
                                                    type="surname"
                                                    placeholder="Potter"
                                                />
                                            </Col>
                                        </Row>

                                        {/* I would change this library if 'react-country-select', I had more time, to another with search input 
                                        ,beacause here you have to select country manually and it's hard to find your country */}
                                        <FormikInput
                                            label="Country"
                                            name="country-code"
                                            type="country-select"
                                            countryCode={countryCode}
                                            setCountryCode={setCountryCode}
                                            setFieldValue={props?.setFieldValue}
                                        />

                                        <FormikInput
                                            label="City"
                                            name="city"
                                            type="city"
                                            placeholder="London"
                                        />

                                        <FormikInput
                                            label="Address"
                                            name="address"
                                            type="address"
                                            placeholder="Diagon Alley 22/5"
                                        />

                                        <Row>
                                            <Col md={6}>
                                                <FormikInput
                                                    label="State"
                                                    name="state"
                                                    type="state"
                                                    placeholder="Enter state"
                                                />
                                            </Col>
                                            <Col md={6}>
                                                <FormikInput
                                                    label="Zipcode"
                                                    name="zipcode"
                                                    type="zipcode"
                                                    placeholder="MK1 9QH"
                                                />
                                            </Col>
                                        </Row>

                                        <FormikInput
                                            label="Email Address"
                                            name="email"
                                            type="email"
                                            placeholder="jane@formik.com"
                                        />

                                        <FormikInput
                                            label="Phone"
                                            name="phone"
                                            type="phone"
                                            defaultCountry={"PL"}
                                            placeholder="Enter phone number"
                                            countryCode={countryCode}
                                            setFieldValue={props?.setFieldValue}
                                            value={props?.values?.phone}
                                        />

                                        <FormikInput
                                            label="Date Of Birth"
                                            name="date-of-birth"
                                            type="date"
                                            placeholder="MM/DD/YYYY"
                                        />

                                    </div>
                                </Col>

                                <Col md={3}>
                                    <Tile id={minifigDetails?.set_num}
                                        name={minifigDetails?.name}
                                        set_img_url={minifigDetails?.set_img_url}
                                    >
                                        <div className='tile-checkout-details'>
                                            <p>There {minifigParts?.length === 1 ? "is" : "are"} {minifigParts?.length} {minifigParts?.length === 1 ? "part" : "parts"} in this minifig:</p>

                                            <div className='tile-checkout-details--parts'>
                                                {minifigParts?.map((part: any) => (
                                                    <a href={part?.part?.part_url} target="__blank" className='tile-checkout-details--parts__part'>
                                                        <img src={part?.part?.part_img_url} />
                                                        <div>
                                                            <div className='text-truncate'>{part?.part?.name}</div>
                                                            <div className='tile-checkout-details--parts__part-number'>{part?.part?.part_num}</div>
                                                        </div>
                                                    </a>
                                                ))}
                                            </div>
                                            <div className='submit-btn-container'>
                                                <Btn
                                                    disabled={isSubmitting || isValidating || isSubmitted}
                                                    isLoading={isFormLoading || isSubmitted}
                                                    onClick={handleSubmit}
                                                    color='primary'
                                                    className={!formIsValid ? "disabled" : ""}
                                                    text="Accio Minifig!"
                                                />
                                            </div>
                                        </div>
                                    </Tile>
                                </Col>
                            </Row>
                        )
                    }}
            </Formik>
            
        ) : (
            <div className="spinner-border text-light" role="status">
                <span className="sr-only"></span>
            </div>
        )}
        </div>
    )
}

export default Checkout
