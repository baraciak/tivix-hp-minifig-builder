import React, { ReactElement } from 'react'
import minifigPlaceholder from "../../../../assets/images/minifig-placeholder.png"

interface Props {
  id: string;
  name: string;
  set_img_url: string;
  set_url?: string;
  isSelected?: boolean;
  onClick?: (set_num: string) => void;
  children?: ReactElement;
}

const Tile = ({
    id,
    name,
    set_img_url,
    set_url,
    onClick,
    isSelected,
    children
}: Props) => {
  return (
    <div className={`tile ${isSelected ? "selected" : "" } ${children ? "tile--show-details" : ""}`} onClick={() => onClick && onClick(id)}>
      {/* @ts-ignore */}
      <img src={set_img_url ? set_img_url : minifigPlaceholder }
          height={150}
          width={150}
          className={!set_img_url ? "img-placeholder" : ""}
          onError={({ currentTarget }: any) => {
            currentTarget.onerror = null;
            currentTarget.src = minifigPlaceholder;
          }}
      />
      <span className="name">
          {children ? name : name.split(",")[0]}
      </span>
      
      {children ? children : (set_url ? <div className='tile-details-button'><a href={set_url} target="__blank" rel="noopener noreferrer">Show Details</a></div> : null)}
    </div>
  )
}

export default Tile
