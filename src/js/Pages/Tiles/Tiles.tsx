import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { Link } from 'react-router-dom';
import Btn from "../../Components/Btn/Btn";
import { getMinifigs, setMinifigId, setRerollCount } from "../../store/actions";
import Tile from './Tile/Tile';
import './Tiles.scss';

interface Minifig {
    last_modified_dt: string;
    name: string;
    num_parts: number;
    set_img_url: string;
    set_num: string;
    set_url: string;
}

export interface Comment {
    body: string;
    email: string;
    id: number;
    name: string;
    postId: number;
}

const Tiles = (props: any) => {
    const dispatch = useDispatch();
    const { rerollCounter } = useSelector((state: any) => state.AppState);
    const setRerollCounter = (count: number) => dispatch(setRerollCount(count))
    const { items, isLoading, selectedMinifigId } = useSelector((state: any) => state.Minifigs);
    const setSelectedMinifigId = (id: string | null) => dispatch(setMinifigId(id))

    useEffect(() => {
        !items?.length && handleGetMinifigs()
    }, [items])

    const handleGetMinifigs = async () => {
        dispatch(getMinifigs())
    }

    const handleClick = (set_num: string) => {
        setSelectedMinifigId(set_num)
    }

    const reroll = () => {
        setSelectedMinifigId(null)
        handleGetMinifigs()
        if(rerollCounter > 0){
            setRerollCounter(rerollCounter - 1)
        }
    }

    return (
        <div className='page-container'>
            {!isLoading && items?.length ? (
                <>
                    <h1 className="homepage-title font-harry-potter mb-4">CHOOSE YOUR MINIFIG</h1>
                    <div className='tiles-container'>
                        {items?.map((minifig: Minifig) => (
                            <Tile key={minifig?.set_num}
                                id={minifig?.set_num}
                                name={minifig?.name}
                                set_img_url={minifig?.set_img_url}
                                set_url={minifig?.set_url}
                                onClick={handleClick}
                                isSelected={minifig?.set_num === selectedMinifigId}
                            />
                        ))}
                    </div>
                    {/* sorry for the font at counter - no time to fix :\ */}
                    <Btn disabled={rerollCounter === 0} onClick={reroll} color='primary' className="mt-5" text={`Reroll (${rerollCounter})`}></Btn>

                    <Btn disabled={!selectedMinifigId} color='primary' className="mt-5">
                        <Link to={`/checkout/${selectedMinifigId}`}>Proceed to checkout</Link>
                    </Btn>
                </>
            ) : (
                <div className="spinner-border text-light" role="status">
                    <span className="sr-only"></span>
                </div>
            )}
        </div>
    )
}

export default Tiles
