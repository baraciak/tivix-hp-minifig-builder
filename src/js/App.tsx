import React from 'react';
import { Provider } from "react-redux";
import {
    BrowserRouter, Route, Routes
} from "react-router-dom";
import questionMark from "../assets/images/question-mark.png";
import Alert from './Components/Alert/Alert';
import Tiles from './Pages/Tiles/Tiles';
import Checkout from './Pages/Checkout/Checkout';
import ErrorPage from './Pages/ErrorPage/ErrorPage';
import Home from './Pages/Home/Home';
import store from './store';

interface Props {
    message?: string;
};

const App = ({}: Props) => {
    return (
        <div className="app-container">
            <Provider store={store}>
                <img src={questionMark} className='question-mark-bg question-mark-bg-1' />
                <img src={questionMark} className='question-mark-bg question-mark-bg-2' />

                <Alert />
                {/* Beacause of simplicity of the project and time shortage I've left router as simple as it can be, 
                In more complex project I would create a list of route objects and AppRoute.tsx with some more logic
                (for ex. is user authorized check, redirect to login page, public/private routes)
                in separate files and then render all the routes here*/}
                <BrowserRouter>
                    <Routes>
                        <Route path="/" element={<Home />} />
                        <Route path="/start" element={<Tiles />} />
                        <Route path="/checkout/:minifig_id" element={<Checkout />} />
                        <Route path="/not-found" element={<ErrorPage />} />
                        <Route path="*" element={<ErrorPage />} />
                    </Routes>
                </BrowserRouter>
            </Provider>
        </div>
    )
}

export default App