import {
  CLEAR_MINIFIGS,
  GET_MINIFIGS,
  GET_MINIFIGS_FAIL,
  GET_MINIFIGS_SUCCESS,
  SET_MINIFIG_ID,
} from "./actionTypes"

const INIT_STATE = {
  items: [],
  isLoading: "",
  selectedMinifigId: null
}

const Minifigs = (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_MINIFIGS:
      return {
        ...state,
        isLoading: true
      }

    case GET_MINIFIGS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        items: action?.payload?.minifigs
      }

    case GET_MINIFIGS_FAIL:
      return {
        ...state,
        isLoading: false,
        items: []
      }

    case CLEAR_MINIFIGS:
      return {
        ...state,
        isLoading: false,
        items: []
      }

    case SET_MINIFIG_ID:
      return {
        ...state,
        selectedMinifigId: action?.payload?.id
      }

    default:
      return state
  }
}

export default Minifigs
