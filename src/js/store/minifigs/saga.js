import { getMinfigs } from "./../../helpers/api"
import { call, put, takeEvery, all } from "redux-saga/effects"
import { getMinifigsSuccess, getMinifigsFail } from "./actions"
import { GET_MINIFIGS } from "./actionTypes"
import { random } from "lodash";

function* getMinifigsAsync({ payload }) {
  try {
    // here I wanted to fetch minifigs as random as it was possible with rebrickable API 
    // Normally in such case an API should just return 3 random minifigs, because fetching 3 times for random index is not the most optimized way

    //get total items from API
    const precall = yield call(() => getMinfigs(1, 1))
    const totalItems = precall?.data?.count;;

    //generate 3 random unique indexes in range of total HP minfiigs found in API
    const randomMinifigsIndexes = []
    while(randomMinifigsIndexes.length < 3){
      const rand = random(1, totalItems);
      if(randomMinifigsIndexes.indexOf(rand) === -1) {
        randomMinifigsIndexes.push(rand);
      }
    }

    //send 3 api calls in parallel 
    const promises = randomMinifigsIndexes?.map((index) => {
      return new Promise((resolve, reject) => {
        resolve(getMinfigs(1, index)); 
      })
    })
    let minifigs = yield all(promises);

    //format minifigs to one array
    minifigs = minifigs?.map((minfigResult) => minfigResult?.data?.results[0])?.flat()

    yield put(getMinifigsSuccess(minifigs))
  } catch (error) {
    yield put(getMinifigsFail(error))
  }
}

function* minifigsSaga() {
  yield takeEvery(GET_MINIFIGS, getMinifigsAsync)
}

export default minifigsSaga