import {
  GET_MINIFIGS,
  GET_MINIFIGS_SUCCESS,
  GET_MINIFIGS_FAIL,
  CLEAR_MINIFIGS,
  SET_MINIFIG_ID
} from "./actionTypes"

export const getMinifigs = (perPage = 10, page = 1) => ({
  type: GET_MINIFIGS,
  payload: { perPage, page }
})

export const getMinifigsSuccess = (minifigs) => ({
  type: GET_MINIFIGS_SUCCESS,
  payload: { minifigs }
})

export const getMinifigsFail = () => ({
  type: GET_MINIFIGS_FAIL
})

export const clearMinifigs = () => ({
  type: CLEAR_MINIFIGS
})

export const setMinifigId = (id) => ({
  type: SET_MINIFIG_ID,
  payload: { id }
})


