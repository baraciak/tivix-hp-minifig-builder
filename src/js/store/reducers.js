import { combineReducers } from 'redux';

import Alert from './alert/reducer';
import Minifigs from './minifigs/reducer';
import AppState from './appState/reducer';
import { RESET_APP_STATE } from './actions';

const allReducers = combineReducers({
    Alert,
    Minifigs,
    AppState
});

//root reducer wrapper
const rootReducer = (state, action) => {
    if (action.type === RESET_APP_STATE) {
      // exclude reducers from reset
    //   const { Layout } = state;
      state = { };
    }
  
    return allReducers(state, action);
};

export default rootReducer;
