import { all, fork } from 'redux-saga/effects';

import minifigsSaga from './minifigs/saga';

export default function* rootSaga() {
    yield all([
        fork(minifigsSaga),
    ]);
}
