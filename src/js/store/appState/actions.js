import {
  SET_REROLL_CONTER
} from "./actionTypes"

export const setRerollCount = (count) => ({
  type: SET_REROLL_CONTER,
  payload: { count }
})


