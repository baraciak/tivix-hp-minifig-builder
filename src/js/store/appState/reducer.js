import {
  SET_REROLL_CONTER,
} from "./actionTypes"

const INIT_STATE = {
  rerollCounter: 3
}

const AppState = (state = INIT_STATE, action) => {
  switch (action.type) {
    case SET_REROLL_CONTER:
      return {
        ...state,
        rerollCounter: action?.payload?.count
      }

    default:
      return state
  }
}

export default AppState
