import {
  OPEN_ALERT,
} from "./actionTypes"

const INIT_STATE = {
  text: "",
  variant: "",
}

const Alert = (state = INIT_STATE, action) => {
  switch (action.type) {
    case OPEN_ALERT:
      return {
        ...state,
        text: action?.payload?.text,
        variant: action?.payload?.variant,
      }

    default:
      return state
  }
}

export default Alert
