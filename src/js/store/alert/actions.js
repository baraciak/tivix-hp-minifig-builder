import {
  OPEN_ALERT
} from "./actionTypes"

export const openAlert = (text, variant) => ({
  type: OPEN_ALERT,
  payload: { text, variant }
})