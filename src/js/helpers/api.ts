import axios from "axios";
import  { random } from "lodash";

const API_URL = "https://rebrickable.com/api/v3";
const REBRICKABLE_TOKEN = "51040ba73af293216972b5aa680d097f"
axios.defaults.baseURL = API_URL;
axios.defaults.headers.common['Authorization'] = "key " + REBRICKABLE_TOKEN;
axios.defaults.headers.common['Accept'] = "application/json";

export const getMinfigs = (perPage = 10, page = 1) => axios.get(`/lego/minifigs/?page=${page}&page_size=${perPage}&in_theme_id=246`)
export const getMinifigDetails = (id: string) => axios.get(`/lego/minifigs/${id}`)
export const getMinifigParts = (id: string) => axios.get(`/lego/minifigs/${id}/parts`)

export const createOrder = async (values: any) => {
    //simulate an API call to create an order
    return new Promise((resolve, reject) => 
        setTimeout(() => {
            const randomInt = random(1, 1000);
            if(randomInt >= 200){
                return resolve({ status: 200, data: { message: "Your Order has been created succefully! // (FAKE API SUCCESS)", status: "success", order_id: randomInt }})
            } else {
                //random error to show frontend error handling
                return reject({ status: 500, data: { message: "Huston, we have a problem. Please, try agian later. // (FAKE API ERROR)", status: "danger", order_id: randomInt }})
            }
        }, 750)
    );
}   